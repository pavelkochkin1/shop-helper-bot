import logging.config
from typing import Any, Dict, List

import telebot
from shops_parser import MVideoParser, WBParser
from shops_parser.exceptions import NoProductsFoundError
from shops_parser.ref import ShopProduct

from shop_helper_bot import config as cfg
from shop_helper_bot.utils import (
    get_caption_text,
    # data_validation,
    get_bad_request_caption,
)
from shop_helper_bot.database import Users
from shop_helper_bot.keyboards import (
    empty_keyboard,
    get_settings_keyboard,
    get_shops_keyboard,
    menu_keyboard,
    start_keyboard,
)

logging.config.fileConfig("src/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger("bot")

bot = telebot.TeleBot(cfg.BOT_TOKEN)
users_db = Users()

parsers: Dict[str, Dict[str, Any]] = {
    "mvideo": {
        "parser": MVideoParser(cfg.USER_AGENT),
        "params": ["sort", "count", "city"],
    },
    "wildberries": {
        "parser": WBParser(cfg.USER_AGENT),
        "params": ["sort", "count"],
    },
}

logger.info("Bot init.")


@bot.message_handler(commands=["start"], content_types=["text"])
def send_welcome(message):
    if users_db.get_attr(message, "user_id") is not None:
        bot.send_message(
            chat_id=message.chat.id,
            text=cfg.REG_AGAIN,
            reply_markup=menu_keyboard,
        )
        bot.register_next_step_handler(message, main_menu)
    else:
        bot.send_message(
            chat_id=message.chat.id,
            text=cfg.START_MSG,
            reply_markup=start_keyboard,
        )

        bot.register_next_step_handler(message, registration)


@bot.message_handler(content_types=["text"])
def unknown_command_error(message):
    if users_db.get_attr(message, "user_id") is not None:
        bot.send_message(
            chat_id=message.chat.id,
            text=cfg.ERROR_MSG,
            reply_markup=menu_keyboard,
        )
        bot.register_next_step_handler(message, main_menu)
    else:
        bot.send_message(
            chat_id=message.chat.id,
            text=cfg.START_MSG,
            reply_markup=start_keyboard,
        )

        bot.register_next_step_handler(message, registration)


def registration(message):
    if message.text == cfg.START_BUTTON:
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.REG_MSG,
            reply_markup=empty_keyboard,
        )
        bot.register_next_step_handler(message, add_new_user)
    else:
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.BAD_START,
            reply_markup=start_keyboard,
        )

        bot.register_next_step_handler(message, registration)


def validate_registration(info: list) -> bool:
    for i, attr in enumerate(users_db.available_attrs):
        type_for_check = cfg.AVAILABLE_USER_ATTR["types"][attr]
        if cfg.data_validation(attr, info[i], type_for_check) is False:
            return False

    return True


def add_new_user(message):
    info = message.text.lower().split()
    if validate_registration(info):
        info = {
            "city": info[0],
            "sort": info[1],
            "count": info[2],
        }
        users_db.add_user(message, info)
        bot.send_message(
            message.chat.id,
            text=cfg.AFTER_REG,
            reply_markup=menu_keyboard,
        )
        logger.info(f"Added new user with user_id={message.chat.id}")

        bot.register_next_step_handler(message, main_menu)
    else:
        message = bot.send_message(
            message.chat.id,
            text=cfg.REG_ERROR,
        )
        logger.error(f"Regestration error with user info: {info}")

        bot.register_next_step_handler(message, add_new_user)


def main_menu(message):
    if message.text == cfg.SEARCH_BUTTON:
        shops_keyboard = get_shops_keyboard()
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.CHOOSE_SHOP,
            reply_markup=shops_keyboard,
        )
        bot.register_next_step_handler(message, search_request_handler)
    elif message.text == cfg.SETTINGS_BUTTON:
        settings_keyboard = get_settings_keyboard(message, users_db)
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.CHOOSE_SETTING_BUTTON,
            reply_markup=settings_keyboard,
        )
        bot.register_next_step_handler(message, change_setting_handler)
    else:
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.UNKNOWN_BUTTON,
            reply_markup=menu_keyboard,
        )
        bot.register_next_step_handler(message, main_menu)


def search_request_handler(message):
    if message.text == cfg.MENU_BUTTON:
        bot.send_message(
            chat_id=message.chat.id,
            text=cfg.MENU_BUTTON,
            reply_markup=menu_keyboard,
        )
        bot.register_next_step_handler(message, main_menu)
    elif message.text in cfg.SHOPS:
        bot.send_message(
            chat_id=message.chat.id,
            text=cfg.LETS_SEARCH,
            reply_markup=empty_keyboard,
        )
        bot.register_next_step_handler(message, search_response, message.text)
    elif message.text in cfg.ALL_SHOPS_BUTTON:
        bot.send_message(
            chat_id=message.chat.id,
            text=cfg.LETS_SEARCH,
            reply_markup=empty_keyboard,
        )
        bot.register_next_step_handler(message, search_response_all_shops)
    else:
        shops_keyboard = get_shops_keyboard()
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.UNKNOWN_BUTTON,
            reply_markup=shops_keyboard,
        )
        bot.register_next_step_handler(message, search_request_handler)


def send_products(message, products: List[ShopProduct]):
    for product in products:
        caption = get_caption_text(product)
        bot.send_photo(
            chat_id=message.chat.id,
            photo=product.image,
            caption=caption,
            reply_markup=menu_keyboard,
        )


def search_response(message, shop_name: str):
    shop_name = shop_name.lower()
    user_info = users_db.show_info(message, parsers[shop_name]["params"])
    user_info["sort"] = cfg.AVAILABLE_USER_ATTR["sort"][user_info["sort"]]
    try:
        data = parsers[shop_name]["parser"].get_data(message.text, **user_info)

        send_products(message, data)
        logger.info(
            ", ".join(
                [
                    "Sent products",
                    f"request: {message.text}",
                    f"shop: {shop_name}",
                    f"params: {user_info}",
                ]
            )
        )

        bot.register_next_step_handler(message, main_menu)

    except NoProductsFoundError:
        bot.send_message(
            chat_id=message.chat.id,
            text=get_bad_request_caption(shop_name),
            reply_markup=empty_keyboard,
        )
        logger.error(
            ", ".join(
                [
                    "NoProductsFoundError",
                    f"request: {message.text}",
                    f"shop: {shop_name}",
                    f"params: {user_info}",
                ]
            )
        )

        bot.register_next_step_handler(message, search_response)


def search_response_all_shops(message):
    data = list()
    for shop, parser in parsers.items():
        user_info = users_db.show_info(message, parser["params"])

        user_info["sort"] = cfg.AVAILABLE_USER_ATTR["sort"][user_info["sort"]]
        user_info["count"] //= 2

        try:
            data.extend(parser["parser"].get_data(message.text, **user_info))
        except NoProductsFoundError:
            bot.send_message(
                chat_id=message.chat.id,
                text=get_bad_request_caption(shop),
                reply_markup=empty_keyboard,
            )
            bot.register_next_step_handler(message, search_response)

    send_products(message, data)
    logger.info(
        ", ".join(
            [
                "Sent products",
                f"request: {message.text}",
                f"shops: {parsers.keys()}",
                f"params: {user_info}",
            ]
        )
    )

    bot.register_next_step_handler(message, main_menu)


def change_setting_handler(message):
    attr = message.text.split(":")[0]
    if message.text == cfg.MENU_BUTTON:
        bot.send_message(
            chat_id=message.chat.id,
            text=cfg.MENU_BUTTON,
            reply_markup=menu_keyboard,
        )
        bot.register_next_step_handler(message, main_menu)
    elif attr in cfg.AVAILABLE_USER_ATTR.keys():
        attr = message.text.split(":")[0]
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.CHOOSE_NEW_SETTING_BUTTON,
            reply_markup=empty_keyboard,
        )

        bot.register_next_step_handler(message, change_setting, attr)
    else:
        settings_keyboard = get_settings_keyboard(message, users_db)
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.UNKNOWN_BUTTON,
            reply_markup=settings_keyboard,
        )
        bot.register_next_step_handler(message, change_setting_handler)


def change_setting(message, attr: str):
    new_value = message.text.lower()
    type_for_check = cfg.AVAILABLE_USER_ATTR["types"][attr]
    if cfg.data_validation(attr, new_value, type_for_check):
        users_db.change_attr(message, attr, new_value)
        logger.info(
            ", ".join(
                [
                    "Setting was changed",
                    f"attr: {attr}",
                    f"new_value: {new_value}",
                ]
            )
        )

        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.SETTING_SET,
            reply_markup=menu_keyboard,
        )
        bot.register_next_step_handler(message, main_menu)
    else:
        message = bot.send_message(
            chat_id=message.chat.id,
            text=cfg.CHOOSE_NEW_SETTING_BUTTON,
            reply_markup=empty_keyboard,
        )
        logger.error(
            ", ".join(
                [
                    "Setting was not changed",
                    f"attr: {attr}",
                    f"new_value: {new_value}",
                ]
            )
        )

        bot.register_next_step_handler(message, change_setting, attr)


def bot_run():
    bot.infinity_polling()


if __name__ == "__main__":
    bot_run()
