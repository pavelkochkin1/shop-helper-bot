from typing import Any, Callable

from shops_parser.ref import ShopProduct
from shop_helper_bot.config import AVAILABLE_USER_ATTR


def data_validation(attr: str, value: Any, to_type: Callable):
    if to_type(value) not in AVAILABLE_USER_ATTR[attr]:
        return False
    return True


def get_caption_text(info: ShopProduct) -> str:
    result = list()
    result.append(info.name)
    result.append(f"Цена: {info.price}")
    result.append(f"{info.link}\n")
    return "\n".join(result)


def get_bad_request_caption(shop: str) -> str:
    return f"По дранному запросу ничего не нащел в {shop}"
