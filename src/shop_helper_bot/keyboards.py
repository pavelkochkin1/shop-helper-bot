from shop_helper_bot.database import Users
from telebot import types

from shop_helper_bot import config as cfg


empty_keyboard = types.ReplyKeyboardRemove()

start_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
start_button = types.KeyboardButton(cfg.START_BUTTON)
start_keyboard.add(start_button)

menu_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
search_button = types.KeyboardButton(cfg.SEARCH_BUTTON)
settings_button = types.KeyboardButton(cfg.SETTINGS_BUTTON)
menu_keyboard.add(search_button, settings_button)


def get_shops_keyboard():
    shops_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    for shop in cfg.SHOPS:
        shop_button = types.KeyboardButton(shop)
        shops_keyboard.add(shop_button)
    shops_keyboard.add(cfg.ALL_SHOPS_BUTTON)
    shops_keyboard.add(cfg.MENU_BUTTON)

    return shops_keyboard


def get_settings_keyboard(message, users_db: Users):
    settings_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    for setting in list(cfg.AVAILABLE_USER_ATTR.keys())[:-1]:
        value = users_db.get_attr(message, setting)
        setting_button = types.KeyboardButton(f"{setting}: {value}")
        settings_keyboard.add(setting_button)
    settings_keyboard.add(cfg.MENU_BUTTON)

    return settings_keyboard
