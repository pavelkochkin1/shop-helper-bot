import sqlite3
from typing import Union, Dict
from shop_helper_bot.config import DB_NAME
from shop_helper_bot.exceptions import UserAlreadyExists


class Users:
    def __init__(self, db_name: str = DB_NAME) -> None:
        self.db_name = db_name

        self.available_attrs = [
            "city",
            "sort",
            "count",
        ]

        self._create_table()

    def _create_table(self):
        with sqlite3.connect(self.db_name) as conn:
            query = """
                CREATE TABLE IF NOT EXISTS users(
                    user_id INT,
                    city TEXT,
                    sort TEXT,
                    count INT
                )
            """
            conn.execute(query)

    def add_user(self, message, attrs: dict):
        with sqlite3.connect(self.db_name) as conn:
            if self.get_attr(message, "city") is None:
                conn.execute(
                    "INSERT INTO users VALUES (?, ?, ?, ?)",
                    (message.chat.id, attrs["city"], attrs["sort"], attrs["count"]),
                )
            else:
                raise UserAlreadyExists

    def get_attr(self, message, attr: str):
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            query = f"""
                SELECT {attr}
                FROM users
                WHERE user_id = ?
            """
            cur.execute(
                query,
                (message.chat.id,),
            )
            value = cur.fetchone()
            if value is None:
                return None

            return value[0]

    def show_info(self, message, attrs_to_get: list) -> Dict[str, Union[str, int]]:
        info = dict()
        for attr in attrs_to_get:
            info[attr] = self.get_attr(message, attr)

        return info

    def change_attr(self, message, attr: str, value: Union[int, str]):
        with sqlite3.connect(self.db_name) as conn:
            cur = conn.cursor()
            query = f"""
                UPDATE users
                SET {attr} = ?
                WHERE user_id = ?
            """
            cur.execute(
                query,
                (value, message.chat.id),
            )
