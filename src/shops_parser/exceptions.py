class NoProductsFoundError(Exception):
    pass


class NoImageFoundError(Exception):
    pass


class WrongCity(Exception):
    pass
