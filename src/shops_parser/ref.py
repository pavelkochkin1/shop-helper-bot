from abc import ABCMeta, abstractmethod
from typing import List, Any
import requests
from urllib.parse import urlparse


class Shop(metaclass=ABCMeta):
    """This is abstract class for shop's parser.

    Methods:
    - get_data:
        - args:
            - product (str): what product parser need to get
        - return:
            - List[ShopProduct]: which contains name, link, image link and price.
    - set_session:
        - args:
            - user_agent (str): for get request
            - headers: if parser need headers to parse
            - cookies (str): if parser need cookies to parse
    """

    @abstractmethod
    def __init__(self, user_agent: str) -> None:
        pass

    @abstractmethod
    def get_data(self, product: str) -> List["ShopProduct"]:
        pass

    @abstractmethod
    def set_session(
        self,
        user_agent: str,
        headers=None,
        cookies: str = None,
        *args: Any,
        **kwargs: Any,
    ) -> None:
        pass


class ShopProduct:
    def __init__(
        self,
        name: str,
        image: str,
        link: str,
        price: int,
        short_link: bool = True,
    ) -> None:

        self.name = name

        if isinstance(image, str) and self.url_validator(image):
            self.image = image
        else:
            raise TypeError("`image` must be `str` and has «link-like» format!")

        if isinstance(link, str) and self.url_validator(link):
            self.link = self.get_short_link(link) if short_link else link
        else:
            raise TypeError("`link` must be `str` and has «link-like» format!")

        self.price = price

    @classmethod
    def from_dict(cls, d: dict, short_link=True) -> "ShopProduct":
        return ShopProduct(
            name=d["name"],
            image=d["image"],
            link=d["link"],
            price=d["price"],
            short_link=short_link,
        )

    @staticmethod
    def get_short_link(link: str) -> str:
        response = requests.get("https://clck.ru/--?url=" + link)
        return response.text

    @staticmethod
    def url_validator(link: str) -> bool:
        result = urlparse(link)
        return all([result.scheme, result.netloc])

    def __str__(self):
        result = list()
        result.append(self.name)
        result.append(str(self.price))
        result.append(self.link)
        return " - ".join(result)

    def __repr__(self):
        result = list()
        result.append(self.name)
        result.append(str(self.price))
        result.append(self.link)
        return " - ".join(result)
