import logging
import logging.config
from typing import List

import requests

from shops_parser.exceptions import NoImageFoundError, NoProductsFoundError
from shops_parser.ref import Shop, ShopProduct

logging.config.fileConfig("src/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger("parser")


class WBParser(Shop):
    def __init__(self, user_agent: str):
        self.set_session(user_agent)

        logger.info("WBParser init.")

    def set_session(
        self,
        user_agent: str,
        *args,
        **kwargs,
    ) -> None:
        self.session = requests.Session()

        self.session.headers.update(
            {
                "accept": "application/json",
                "accept-language": "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7",
                "user-agent": user_agent,
            }
        )

    def get_data(
        self,
        product: str,
        sort: str = "pop",
        count: int = 5,
    ) -> List[ShopProduct]:

        url = "https://search.wb.ru/exactmatch/ru/common/v4/search?appType=1"
        dest = "dest=-1075831,-72192,-1760251,-3115289"
        locale = "locale=ru"
        q = f"query={'+'.join(product.split())}"
        res_set = "resultset=catalog"
        sort = f"sort={self.sort_to_command(sort)}"
        spell = "suppressSpellcheck=false"

        response = self.session.get(
            "&".join([url, dest, locale, res_set, spell, q, sort])
        ).json()
        logger.debug("Get response with products' info.")

        try:
            products = response["data"]["products"][:count]
        except Exception:
            raise NoProductsFoundError(
                f"`{product}` was not found in the `{self.__class__.__name__}`."
            )
        logger.debug("Make list with products info")

        products_info = list()

        for item in products:
            id = str(item["id"])
            link = f"https://www.wildberries.ru/catalog/{id}/detail.aspx?targetUrl=XS"
            name = item["name"]
            price = item["salePriceU"] // 100
            image = self.get_image_link(id)
            products_info.append(
                ShopProduct(
                    name=name,
                    image=image,
                    link=link,
                    price=price,
                )
            )

        logger.info(f"Return products' data for product=`{product}`")
        return products_info

    def make_image_url(self, basket, id: str) -> str:
        v = id[:-5]
        p = id[:-3]
        img = f"https://basket-{basket}.wb.ru/vol{v}/part{p}/{id}/images/big/1.jpg"
        return img

    def search_image_in_basket(self, id: str, baskets: list) -> str:
        for basket in baskets:
            url = self.make_image_url(basket, id)
            response = requests.get(url)
            if response.status_code == 200:
                return url
        raise NoImageFoundError("No found image")

    def get_image_link(self, id: str):
        baskets = ["01", "02", "03", "04", "05", "07", "08"]
        if len(id) > 8:
            return self.search_image_in_basket(id, baskets[-2:])
        if len(id) <= 8:
            return self.search_image_in_basket(id, baskets[:-2])

    def sort_to_command(self, sort: str) -> str:
        s = {"pop": "popular", "desc": "pricedown", "asc": "priceup"}
        if sort not in s:
            raise ValueError(f"{sort} is not in the list of available sorts.")

        return s[sort]


if __name__ == "__main__":
    with open("src/shops_parser/user_agent.txt", "r") as f:
        user_agent = f.readline()

    parser = WBParser(user_agent=user_agent)
    for prod in parser.get_data(product="iphone 12", count=5, sort="pop"):
        print(prod)
