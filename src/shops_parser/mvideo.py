import logging
import logging.config
from typing import List, Optional

import requests

from shops_parser.exceptions import NoProductsFoundError, WrongCity
from shops_parser.ref import Shop, ShopProduct


logging.config.fileConfig("src/logging.conf", disable_existing_loggers=False)
logger = logging.getLogger("parser")


class MVideoParser(Shop):
    def __init__(self, user_agent: str) -> None:
        self.city_code = {
            "москва": "CityCZ_975",
            "санкт-петербург": "CityCZ_975",
            "казань": "CityCZ_975",
        }
        self.user_agent = user_agent

        self.set_session(user_agent)

        logger.info("MvideoParser init.")

    def set_session(
        self,
        user_agent: str,
        *args,
        **kwargs,
    ) -> None:
        self.session = requests.Session()

        self.session.headers.update(
            {
                "accept": "application/json",
                "accept-language": "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7",
                "user-agent": user_agent,
                "referer": "https://www.mvideo.ru/",
            }
        )

        # get cookies
        self.session.get("https://www.mvideo.ru")

    def city2code(
        self,
        city: str,
        *args,
        **kwargs,
    ) -> str:
        if city not in self.city_code:
            raise WrongCity(f"Choose from this cities: {list(self.city_code.keys())}")

        return self.city_code[city]

    def get_data(
        self,
        product: str,
        sort: str = "pop",
        count: int = 5,
        city: str = "москва",
    ) -> List[ShopProduct]:
        self.set_session(self.user_agent)

        self.session.cookies["MVID_CITY_ID"] = self.city2code(city)

        products_ids = self._get_products_ids(
            product=product,
            sort=sort,
            count=count,
        )
        logger.debug("Get products' indexes")

        products_list = self._get_products_list(products_ids)
        logger.debug("Get products' list")

        products_prices = self._get_prices(products_ids)
        logger.debug("Get products' prices")

        merged_data = self._merge_data(
            products_list=products_list,
            products_prices=products_prices,
        )
        logger.info(f"Return merged products' data for product=`{product}`")

        return merged_data

    def _get_products_ids(
        self,
        product: str,
        sort: str,
        count: int,
    ) -> List[str]:

        url = "https://www.mvideo.ru/bff/products/search?"
        response = self.session.get(
            url,
            params={
                "query": "+".join(product.split()),
                "sort": self.sort_to_command(sort),
                "doTranslit": "true",
                "filterParams": [
                    "WyJ0b2xrby12LW5hbGljaGlpIiwiIiwiZGEiXQ==",
                ],  # только в наличии
                "offset": "0",
            },
        ).json()

        if response["body"]["url"]:
            new_url = response["body"]["url"]
            response = self.session.get(url=f"{url}query={product}/{new_url}").json()

        ids: List = response["body"]["products"]

        if not ids:
            raise NoProductsFoundError(
                f"`{product}` was not found in the `{self.__class__.__name__}`."
            )

        return ids[:count]

    def _get_products_list(self, products_ids: List[str]) -> List[dict]:
        json_data = {
            "productIds": products_ids,
        }

        url = "https://www.mvideo.ru/bff/product-details/list"
        response = self.session.post(
            url,
            json=json_data,
        ).json()

        products_list: List[dict] = response["body"]["products"]

        return products_list

    def _get_prices(self, products_ids: List[str]) -> List[dict]:
        params = {
            "productIds": ",".join(products_ids),
            "isPromoApplied": "true",
        }

        url = "https://www.mvideo.ru/bff/products/prices"
        response = self.session.get(
            url,
            params=params,
        ).json()

        prices_list: List[dict] = response["body"]["materialPrices"]

        return prices_list

    def _merge_data(
        self,
        products_list: List[dict],
        products_prices: List[dict],
    ) -> List[ShopProduct]:
        products_info = list()

        for i, item in enumerate(products_list):
            products_info.append(
                ShopProduct(
                    name=item["name"],
                    image=f"https://img.mvideo.ru/{item['image']}",
                    link=f"https://mvideo.ru/products/{item['productId']}",
                    price=products_prices[i]["price"]["salePrice"],
                )
            )

        return products_info

    def sort_to_command(self, sort: str) -> Optional[str]:
        s = {"pop": None, "desc": "price_desc", "asc": "price_asc"}
        if sort not in s:
            raise ValueError(f"`{sort}` is not in the list of available sorts.")

        return s[sort]


if __name__ == "__main__":
    with open("src/shops_parser/user_agent.txt", "r") as f:
        user_agent = f.readline()

    parser = MVideoParser(user_agent)
    for prod in parser.get_data("play station 5", count=5, sort="pop", city="казань"):
        print(prod)
