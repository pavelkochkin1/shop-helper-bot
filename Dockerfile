FROM python:3.9-slim

RUN mkdir /bot

COPY src/ bot/src
COPY pyproject.toml bot/
COPY poetry.lock bot/

WORKDIR /bot

ENV BOT_TOKEN=$BOT_TOKEN

RUN pip3 install poetry
RUN poetry install --no-dev

ENTRYPOINT ["poetry", "run"]
CMD ["bot"]