[![pipeline status](https://gitlab.com/pavelkochkin1/shop-helper-bot/badges/main/pipeline.svg)](https://gitlab.com/pavelkochkin1/shop-helper-bot/-/commits/main)
[![coverage report](https://gitlab.com/pavelkochkin1/shop-helper-bot/badges/main/coverage.svg)](https://gitlab.com/pavelkochkin1/shop-helper-bot/-/commits/main)

# shop-helper-bot - бот для поиска товаров в разных магазинах.

## To-Do
- [x] Мвидео парсер
- [x] Wildberries парсер
- [x] тесты для парсеров
- [x] gitlab-ci пайплайн
- [x] бот
- [x] деплой
- [x] возможность изменять настройки
- [ ] избранные товары 

## How to run:
1. Установить и запустить Docker
2. Добавить в окружение BOT_TOKEN={token}, USER_AGENT={agent}
   Например через файл setenv.sh <- export BOT_TOKEN=2253... 
3. Соберите образ `source ./docker_build.sh`
4. Запустите контейнер `source ./docker_run.sh`

## Исходный код:
* [simpsons_baseline.ipynb](simpsons_baseline.ipynb) contains my research
* [src/shop_helper_bot/](src/shop_helper_bot/) содержит логику бота
* [src/shops_parser](src/shops_parser/) содержит логику парсеров
* [tests/](tests/) тесты использованные при разработке

## Example
[Видео с примером использования](https://www.youtube.com/watch?v=B2lRR7ueWk4)
