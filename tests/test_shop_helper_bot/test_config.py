from src.shop_helper_bot.utils import data_validation


def test_data_validation():
    assert data_validation("sort", "популярное", str) is True
    assert data_validation("city", "москва", str) is True
    assert data_validation("count", 10, int) is True
    assert data_validation("sort", "по убыванию", str) is False
    assert data_validation("city", "пермь", str) is False
    assert data_validation("count", 20, int) is False
