import pytest
from src.shop_helper_bot.database import Users
from tempfile import TemporaryDirectory


class Chat:
    def __init__(self):
        self.id = "12345"


class Message:
    def __init__(self):
        self.chat = Chat()
        self.text = "test"


@pytest.fixture  # 1
def session():
    with TemporaryDirectory() as tmpdirname:
        user_db = Users(db_name=tmpdirname + "tmp.db")
        yield user_db  # 5


@pytest.fixture
def message():
    message = Message()
    yield message


@pytest.fixture
def info():
    info = {
        "city": "москва",
        "sort": "популярное",
        "count": 5,
    }
    yield info


def test_add_user(session, message, info):
    session.add_user(message, info)
    assert session.get_attr(message, "city") == "москва"


def test_show_info(session, message, info):
    session.add_user(message, info)
    assert session.show_info(message, ["city", "sort", "count"]) == info


def test_change_attr(session, message, info):
    session.add_user(message, info)
    session.change_attr(message, "city", "казань")
