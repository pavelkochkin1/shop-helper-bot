import pytest
from shops_parser import WBParser
from shops_parser.exceptions import NoImageFoundError, NoProductsFoundError

with open("src/shops_parser/user_agent.txt", "r") as f:
    user_agent = f.readline()

parser = WBParser(user_agent)


def test_get_data():
    prod_list = parser.get_data(
        product="iphone 13",
        count=5,
        sort="pop",
    )
    assert len(prod_list) == 5

    with pytest.raises(
        NoProductsFoundError, match="`leaveaer` was not found in the `WBParser`."
    ):
        parser.get_data("leaveaer", count=2)


def test_get_image_link():
    with pytest.raises(NoImageFoundError):
        parser.get_image_link("123")


def test_sort():
    with pytest.raises(ValueError):
        parser.sort_to_command("dess")
