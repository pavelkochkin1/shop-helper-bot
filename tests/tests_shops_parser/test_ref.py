from shops_parser.ref import ShopProduct
import pytest


@pytest.fixture
def iphone():
    return ShopProduct(
        name="Iphone",
        image="https://link.ru",
        link="https://link-too.ru",
        price=50,
        short_link=False,
    )


def test_init(iphone):
    assert iphone.image == "https://link.ru"

    with pytest.raises(
        TypeError, match="`link` must be `str` and has «link-like» format!"
    ):
        ShopProduct(
            name="a",
            image="https://link.ru",
            link="aga",
            price=50,
        )

    with pytest.raises(
        TypeError, match="`image` must be `str` and has «link-like» format!"
    ):
        ShopProduct(
            name="a",
            image="hwdgwe.2",
            link="https://link.ru",
            price=50,
        )


def test_str(iphone):
    assert str(iphone) == "Iphone - 50 - https://link-too.ru"


def test_url_validator():
    assert ShopProduct.url_validator("palka") is False


def test_from_dict():
    sp = {
        "name": "a",
        "image": "https://www.ahaha1.ru",
        "link": "https://www.ahaha2.ru",
        "price": 10,
    }
    a = ShopProduct.from_dict(sp, short_link=False)
    assert a.link == "https://www.ahaha2.ru"
