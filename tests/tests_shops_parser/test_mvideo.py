import pytest
from shops_parser import MVideoParser
from shops_parser.exceptions import NoProductsFoundError, WrongCity


with open("src/shops_parser/user_agent.txt", "r") as f:
    user_agent = f.readline()

parser = MVideoParser(user_agent)


def test_get_products_ids():
    assert len(parser._get_products_ids("iphone", sort="pop", count=5)) == 5


def test_city2code():
    assert parser.city2code("москва") == "CityCZ_975"

    with pytest.raises(WrongCity):
        parser.city2code("лапландия")


def test_get_products_list():
    ids = parser._get_products_ids("nvidia", sort="desc", count=5)
    prod_list = parser._get_products_list(ids)
    assert len(prod_list) == 5
    assert "name" in prod_list[0]
    assert "image" in prod_list[0]


def test_get_products_prices():
    ids = parser._get_products_ids("pc", sort="asc", count=1)
    prod_prices = parser._get_prices(ids)
    assert len(prod_prices) == 1
    assert (
        "basePrice" in prod_prices[0]["price"] or "salePrice" in prod_prices[0]["price"]
    )


def test_get_data():
    prod_info = parser.get_data("play startion", sort="desc", count=5)
    assert len(prod_info) == 5

    with pytest.raises(
        NoProductsFoundError, match="`ahaha` was not found in the `MVideoParser`."
    ):
        parser.get_data("ahaha", count=2)


def test_sort():
    with pytest.raises(ValueError):
        parser.sort_to_command("dess")
